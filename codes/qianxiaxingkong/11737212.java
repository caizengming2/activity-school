/**
 * 冒泡排序函数
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
    // 外层循环控制排序轮数
    for (int i = 0; i < n - 1; i++) {
        // 内层循环控制每轮排序的次数
        for (int j = 0; j < n - i - 1; j++) {
            // 比较相邻两个元素的大小，如果前一个元素大于后一个元素，则交换它们的位置
            if (a[j] > a[j + 1]) {
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
        }
    }
}


